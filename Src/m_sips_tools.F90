!> \author Murat Keceli 
!> \url https://bitbucket.org/keceli/qetsc
!> It is based on PETSc, https://www.mcs.anl.gov/petsc/,
!> and SLEPc, http://slepc.upv.es/, packages.
!> 
!> \license 2-clause BSD license
!>  Copyright (c) 2016-2017, Murat Keceli 
!>  All rights reserved.
!>  
!>  Redistribution and use in source and binary forms, with or without modification,
!>  are permitted provided that the following conditions are met:
!>  
!>  * Redistributions of source code must retain the above copyright notice, this
!>    list of conditions and the following disclaimer.
!>  * Redistributions in binary form must reproduce the above copyright notice, this
!>    list of conditions and the following disclaimer in the documentation and/or
!>    other materials provided with the distribution.
!>  
!>  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
!>  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
!>  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
!>  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
!>  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
!>  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
!>  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
!>  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
!>  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
!>  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!>  


MODULE m_sips_tools
    IMPLICIT NONE
    INTEGER, PARAMETER   :: dp = KIND(0.d0)
    LOGICAL, PARAMETER   :: debugging = .FALSE.

CONTAINS

    !> \brief Portable way of getting infinity
    ! from https://groups.google.com/forum/#!topic/comp.lang.fortran/dD4vFNhbO6o
    REAL(dp) FUNCTION get_infinity()
        IMPLICIT NONE
        CHARACTER(LEN=3) :: inf = "INF"
 
        READ(inf,*) get_infinity
    END FUNCTION


    !> Returns true/false after checking file's existence
    LOGICAL FUNCTION get_isfile(filename) 
        IMPLICIT NONE
        CHARACTER(len=*), INTENT(IN) :: filename

        INQUIRE(file=TRIM(filename), exist=get_isfile)
        RETURN
    END FUNCTION get_isfile


    !> \brief Returns evenly spaced array with n elements, starting with `first`, ending with `last`.
    !> replacement got get_uniformsubs
    FUNCTION get_linspace(first,last,n) RESULT(subs)
        REAL(dp), INTENT(IN)          :: first
        REAL(dp), INTENT(IN)          :: last
        INTEGER, INTENT(IN)          :: n
        REAL(dp) :: subs(n)
        REAL(dp) :: step
        INTEGER :: i
        
        step = (last-first) / (n - 1)
        DO i = 1, n
            subs(i) = first + (i-1) * step
        END DO
        RETURN
    END FUNCTION get_linspace


    !> \brief Retuns the location index of a target value in a given array.
    !> Returns the index of the first match within the given order of the array.
    !> Returns 0 if not found.
    FUNCTION get_index(arr,val) RESULT(idx)
        INTEGER, INTENT(IN)          :: arr(:)
        INTEGER, INTENT(IN)          :: val
        INTEGER :: idx
        INTEGER :: i,n
        n = SIZE(arr)
        idx = 0
        DO i=1,n
            IF (arr(i) == val) THEN
                idx = i
                EXIT
            END IF
        END DO
        RETURN
    END FUNCTION

    !> \brief Computes estimated eigenvalues based on inertias and shifts.
    !> Estimation is very simple, if the number of eigenvalues
    !> between shift 0. and shift 1. is 2. The eigenvalues are put on 0 and 1.
    SUBROUTINE inertias_to_eigenvalues(nshift,nreq,shifts,inertias,evals)
        IMPLICIT NONE
        INTEGER, INTENT(IN)  :: nshift !Number of inertia calculations, before solve: nsub+1
        INTEGER, INTENT(IN)  :: nreq ! Number of eigenvalues
        REAL(dp),INTENT(IN)  :: shifts(nshift)
        INTEGER, INTENT(IN)  :: inertias(nshift)
        REAL(dp),INTENT(OUT) :: evals(nreq)
        INTEGER              :: i,j,k,ne,first,last
        INTEGER              :: nevals(nshift-1), nmiss
        REAL(dp)             :: step,widths(nshift-1),twidth,meansep
        k = 0
        last = nshift-1
        IF (shifts(1) < -1.d10) THEN ! first shift at -infinity
            first = 2
        ELSE
            first = 1
        END IF
        twidth = shifts(nshift) - shifts(first)
        meansep = twidth / nreq
        nmiss = inertias(first)
        IF (nmiss > 0) THEN
            DO i = nmiss, 1, -1
                k = k + 1
                evals(k) = shifts(first) - i * meansep * 2.d0
            END DO
        END IF
        DO i=1,nshift-1
            nevals(i) = inertias(i+1) - inertias(i)
            widths(i) = shifts(i+1) - shifts(i)
        END DO
        DO i=first,nshift-1
            ne = nevals(i)
            IF (ne == 0) THEN
                CYCLE
            ELSE IF (ne == 1) THEN
                k = k + 1
                evals(k) = shifts(i)
            ELSE
                step = widths(i) / DBLE(ne)
                DO j = 1, ne
                    k = k + 1
                    IF (k >= nreq) THEN
                        last = i
                        EXIT 
                    END IF
                    evals(k) = shifts(i) + (j-1) * step
                END DO   
            END IF
            IF (k >= nreq) THEN
                last = i
                EXIT 
            END IF
        END DO
        nmiss = nreq - inertias(nshift) 
        IF (nmiss > 0) THEN
            DO i = 1, nmiss
                k = k + 1
                evals(k) = shifts(nshift) + i * meansep * 2.d0
            END DO
        ELSE
            evals(nreq) = shifts(last+1)
        END IF
    END SUBROUTINE inertias_to_eigenvalues


    !> \brief Returns the eigenvalue index of the largest gap, in a given array of eigenvalues.
    !> If the gap id matches any id in given ids, return the index of the next largest gap.
    !> id corresponds to index of the larger eigenvalue on the right side of the gap
    FUNCTION get_gap_id(eigs,ids) RESULT(id)
        REAL(dp), INTENT(IN)         :: eigs(:)
        INTEGER, INTENT(IN),OPTIONAL :: ids(:)
        INTEGER :: id
        INTEGER :: i,n,idid
        REAL(dp), ALLOCATABLE :: dists(:)
        n = SIZE(eigs)
        ALLOCATE(dists(n-1))
        DO i=1,n-1
            dists(i) = eigs(i+1) - eigs(i)
        END DO
        id = MAXLOC(dists,dim=1) + 1
        IF (PRESENT(ids)) THEN
            DO WHILE (.TRUE.)
                idid = get_index(ids,id)
                IF (idid>0) THEN
                    id = MAXLOC(dists,dim=1,mask=(dists<dists(id-1))) + 1
                ELSE
                    EXIT
                END IF
            END DO
        END IF
        RETURN
    END FUNCTION


    !> Returns the number of gaps for a given array of eigenvalues and gap size.
    FUNCTION get_number_of_gaps(eigs,gap) RESULT(ngap)
        IMPLICIT NONE
        REAL(dp),  INTENT(IN) :: eigs(:)
        REAL(dp), INTENT(IN) :: gap
        INTEGER   :: ngap
        INTEGER   :: i,neig
        REAL(dp)  :: dist
        ngap = 0
        neig = SIZE(eigs)
        DO i=1,neig-1
            dist = eigs(i+1) - eigs(i)
            IF (dist > gap) THEN
                ngap = ngap + 1
            ENDIF
        END DO
        RETURN
    END FUNCTION


    !> \brief Returns the maximum absolute difference of two 1D array of reals.
    FUNCTION get_max_abs_diff(arr1,arr2) RESULT(diff)
        IMPLICIT NONE
        REAL(dp), INTENT(IN)          :: arr1(:)
        REAL(dp), INTENT(IN)          :: arr2(:)
        REAL(dp) :: diff
        IF (SIZE(arr1) /= SIZE(arr2)) THEN
            diff = -1.0_dp
        ELSE
            diff = MAXVAL(ABS(arr1-arr2))
        END IF
        RETURN
    END FUNCTION 
    
     
    !> \brief Returns the average seperation between eigenvalues
    FUNCTION get_mean_seperation(x,opt_inertias) RESULT(mean)
        IMPLICIT NONE
        REAL(dp),INTENT(IN)          :: x(:)            !> Eigenvalues or shifts (if inertias are given)
        INTEGER,INTENT(IN),OPTIONAL :: opt_inertias(:) !> Inertias (if given, should match the size of shifts)
        REAL(dp)                     :: mean
        INTEGER  :: n
        REAL(dp) :: dist
        INTEGER  :: first

        n = SIZE(x)
        IF(n<2) THEN
            mean = 1.0_dp
            RETURN
        END IF
        IF (x(1) > -1.D10 ) THEN
            first = 1
        ELSE ! x(1) is -inf
            first = 2
        END IF
        dist = x(n) - x(first)
        IF (debugging) THEN
            IF (dist < 0 ) STOP 'ERROR: get_mean_seperation, dist < 0' 
        END IF
        IF(PRESENT(opt_inertias)) THEN
            IF (debugging) THEN
                IF (SIZE(opt_inertias) /= SIZE(x)) STOP 'ERROR: get_meanseperation, Sizes do not match' 
            END IF
            n = opt_inertias(n) - opt_inertias(1)
        END IF 
        mean = dist / n
        RETURN
    END FUNCTION get_mean_seperation
    



    !> \brief Given a 1D array of real numbers, `evals`, return the indices of
    !> `k` clusters, with uniform population distribution.
    !> Indices correspond to lowest number in the cluster.
    FUNCTION get_cluster_ids_uniform_population(evals,k) RESULT(ids)
        IMPLICIT NONE
        REAL(dp), INTENT(IN)          :: evals(:)
        INTEGER, INTENT(IN)           :: k
        INTEGER                       :: ids(k)
        INTEGER  :: d, i,j, n, r 

        n = SIZE(evals)
        d = n / k
        r = MOD(n, k)
        ids(1) = 1
        j = 1
        DO i = 1, k-1
            IF (j < r) THEN
                ids(i+1)    = i*d + j + 1 
                j           =  j  + 1
            ELSE
                ids(i+1)    = i*d + r + 1 
            END IF
        END DO
    END FUNCTION get_cluster_ids_uniform_population

    
    !> \brief Given a 1D array of real numbers, `evals`, return the indices of
    !> `k` clusters, by using k-means clustering algorithm.
    !> Indices correspond to lowest number in the cluster.
    !> k > 1
    FUNCTION get_cluster_ids_kmeans(n,evals,k,ids0) RESULT(ids)
        IMPLICIT NONE
        INTEGER, INTENT(IN)           :: n        ! Number of elements in the given 1D array    
        REAL(dp), INTENT(IN)          :: evals(n) ! 1D array input
        INTEGER, INTENT(IN)           :: k        ! Number of clusters
        INTEGER, INTENT(IN)           :: ids0(k)  ! The first element ids of each cluster
        INTEGER                       :: ids(k)
        INTEGER                       :: oldids(k)
        INTEGER  ::  i  
        REAL(dp) :: points(k)
        REAL(dp) :: oldpoints(k)
        REAL(dp) :: midpoints(k-1)
        LOGICAL  :: masker(n)
        
        IF (k < 2) THEN
            STOP "Number of clusters i.e. slices should be more than 1"
        END IF
        oldids(:) = 0
        IF (ids0(1) /= 1) STOP 'Error in k-means, ids(1) should be 1'
        ids(:)    = ids0(:)
        points(:) = evals(ids(:))
        DO WHILE(ANY( (ids-oldids) .NE. 0))
            midpoints = (points(2:k) + points(1:k-1)) / 2
            oldpoints(:) = points(:)
            oldids(:)    = ids(:)
            masker(:)    = (evals > midpoints(k-1)) 
            IF (COUNT(masker) == 0 ) THEN
                STOP 'Zero count in kmeans'
            END IF
            points(k)    = SUM(evals, MASK = masker) / COUNT(masker)
            ids(k)       = MINLOC(evals, MASK=masker, DIM=1)
            DO i=2,k-1
                masker(:) = ( (evals > midpoints(i-1)) .AND. (evals <= midpoints(i)) ) 
                IF (COUNT(masker) == 0 ) THEN
                    STOP 'Zero count in kmeans'
                END IF
                points(i) = SUM(evals,MASK = masker) / COUNT(masker)
                ids(i)    = MINLOC(evals,MASK = masker, DIM=1)
            END DO
        END DO
        RETURN
    END FUNCTION get_cluster_ids_kmeans

   
    !> \brief Returns `k+1` subintervals for given cluster `ids` and `evals`.
    !> Slice boundaries are placed on the right of the largest eigenvalue smaller than
    !> the eigenvalue corresponding to a cluster (smallest eval in the cluster).
    FUNCTION get_subs_from_cluster_ids(buffer,subbuffer,k,ids,n,evals) result(subs)
        IMPLICIT NONE
        REAL(dp), INTENT(IN)      :: buffer
        REAL(dp), INTENT(IN)      :: subbuffer
        INTEGER, INTENT(IN)       :: k
        INTEGER, INTENT(IN)       :: ids(k)
        INTEGER, INTENT(IN)       :: n
        REAL(dp), INTENT(IN)      :: evals(n)
        REAL(dp)                  :: subs(k+1)
        INTEGER  :: i  
        
        subs(1)   = evals(1) - 2 * buffer
        subs(k+1) = evals(n) + buffer
        DO i=2, k
            subs(i) = evals(ids(i)) - subbuffer ! If slepc searches from left to right
        END DO
        RETURN
    END FUNCTION 


    !> \brief Returns the given array as sorted in increasing order.
    SUBROUTINE sort_array(x)
        IMPLICIT NONE
        INTEGER,INTENT(INOUT)          :: x(:)            !> Eigenvalues or shifts (if inertias are given)
        INTEGER  :: i,n,tmp,minindex,curmin
        !curmin = -INT(get_infinity())
        curmin = MINVAL(x,dim=1)
        n = SIZE(x)
        DO i=1, n
            minindex = MINLOC(x(i:n),dim=1,mask=(x(i:n)>=curmin)) + i - 1
            curmin = x(minindex)
            tmp = x(i)
            x(i) = x(minindex)
            x(minindex) = tmp
        END DO
        RETURN
    END SUBROUTINE 


    !> Given 'n' eigenvalues, 'eigs', return index of eigenvalues that
    !> are seperated with a distance larger than a given 'gap' size.
    !> gapids correspond to id of the larger eigenvalue on the right side 
    !> the gap
    SUBROUTINE find_gaps(eigs,gap,gapids)
        IMPLICIT NONE
        REAL(dp),  INTENT(IN) :: eigs(:)
        REAL(dp), INTENT(IN)  :: gap
        INTEGER             :: i,ngap,neig
        REAL(dp)            :: dist
        INTEGER, ALLOCATABLE,  INTENT(OUT) :: gapids(:)
        ngap = 0
        neig = SIZE(eigs)
        DO i=1,neig-1
            dist = eigs(i+1) - eigs(i)
            IF (dist > gap) THEN
                ngap = ngap + 1
            ENDIF
        END DO
        ALLOCATE(gapids(ngap))
        ngap = 0
        DO i=1,neig-1
            dist = eigs(i+1) - eigs(i)
            IF (dist > gap) THEN
                ngap = ngap + 1
                gapids(ngap) = i+1
            ENDIF
        END DO
    END SUBROUTINE

    
    !> \brief Removes duplicate elements of an integer array and creates a new array.
    !> If original array is not sorted in increasing order, 'sorted'
    !> argument should be set to .false..
    SUBROUTINE prune_array_duplicates(a,sorted,b)
        IMPLICIT NONE
        INTEGER,  INTENT(INOUT) :: a(:)
        LOGICAL, INTENT(IN) :: sorted
        INTEGER, ALLOCATABLE, INTENT(OUT) :: b(:)
        INTEGER :: m,i,k,dup
        IF (.NOT. SORTED) THEN
            CALL sort_array(a)
        END IF
        m = SIZE(a)
        dup = 0
        k = 1
        DO i=1,m-1
            IF (a(i+1) - a(i) == 0) THEN
                dup = dup + 1
            END IF
        END DO
        ALLOCATE(b(m-dup))
        IF (dup > 0) THEN
            DO i=1,m-1
                IF (a(i+1) - a(i) > 0) THEN
                    b(k) = a(i)
                    k = k + 1
                END IF
            END DO
            b(k) = a(m)
        ELSE
            b = a
        END IF
    END SUBROUTINE

    
    !> \brief Generates a new integer array that concatanates given array b to the tail of 
    !> given array a.
    SUBROUTINE merge_arrays(a,b,c)
        IMPLICIT NONE
        INTEGER,  INTENT(IN) :: a(:)
        INTEGER,  INTENT(IN) :: b(:)
        INTEGER, ALLOCATABLE, INTENT(OUT) :: c(:)
        INTEGER :: m,n,t
        m = size(a)
        n = SIZE(b)
        t = m + n
        ALLOCATE(c(t))
        c(1:m) = a 
        c(m+1:t) = b
    END SUBROUTINE


    !> \brief Computes subintervals `subs` for a given clustering/subinterval type for k slices, interval, and options for slicing.
    !> Eigenvalues can be used to optimize subintervals for better load balance.
    SUBROUTINE compute_subintervals(nsub,subtype,lefttype,buffer,subbuffer,dthresh,gap,interval,evals,ngap,subs) 
        IMPLICIT NONE
        INTEGER,  INTENT(IN)     :: nsub           !> Number of slices
        INTEGER,  INTENT(IN)     :: subtype        !> Slicing type
        INTEGER,  INTENT(IN)     :: lefttype       !> Left boundary type (-inf if > 0)
        REAL(dp), INTENT(IN)     :: buffer         !> Buffer value to adjust global interval
        REAL(dp), INTENT(IN)     :: subbuffer      !> Buffer value to adjust subintervals
        REAL(dp), INTENT(IN)     :: dthresh        !> Threshold for degeneracy
        REAL(dp), INTENT(IN)     :: gap            !> Minimum gap size
        REAL(dp), INTENT(IN)     :: interval(2)    !> Solution interval
        REAL(dp), INTENT(IN)     :: evals(:)       !> Eigenvalues array
        INTEGER,  INTENT(OUT)    :: ngap           !> Number of gaps
        REAL(dp),  INTENT(OUT)   :: subs(nsub+1)   !> Subintervals array
        INTEGER:: i,k,n,first,newid
        INTEGER :: ids0(nsub)
        INTEGER :: ids(nsub)
        ids  = 1
        ids0 = 1
        IF (nsub == 1) THEN
            subs = interval
            RETURN
        END IF
        IF (lefttype > 0) THEN
            first = 2
            k = nsub - 1
            subs(1)  = -get_infinity()
        ELSE
            first = 1
            k = nsub
        END IF
        n = SIZE(evals)
        ngap = get_number_of_gaps(evals, gap)
        SELECT CASE (subtype)
        CASE (0)  ! Equally spaced subintervals
            subs(first:nsub+1) = get_linspace(evals(1)-buffer,evals(n)+buffer,nsub+2-first)
        CASE (1)  ! Equally populated subintervals
            ids(first:nsub)    = get_cluster_ids_uniform_population(evals,k)
            CALL fix_cluster_ids(ids(first:nsub),evals,dthresh)
            subs(first:nsub+1) = get_subs_from_cluster_ids(buffer,subbuffer,k,ids(first:nsub),n,evals) 
        CASE (2)  ! K-means after equally populated subintervals 
            ids0(first:nsub)   = get_cluster_ids_uniform_population(evals,k)
            ids(first:nsub)    = get_cluster_ids_kmeans(n,evals,k,ids0(first:nsub))
            subs(first:nsub+1) = get_subs_from_cluster_ids(buffer,subbuffer,k,ids(first:nsub),n,evals) 
        CASE (4) ! Gap sensitive equal population subintervals
            IF (ngap > INT(nsub/2))  THEN
                ngap = INT(nsub/2)
            END IF
            ids(first:nsub-ngap) = get_cluster_ids_uniform_population(evals,k-ngap)
            DO i=1, ngap
                newid = get_gap_id(evals,ids(first:nsub-ngap+i-1))
                ids(nsub-ngap+i) = newid
            END DO
            CALL sort_array(ids)
            CALL fix_cluster_ids(ids(first:nsub),evals,dthresh)
            subs(first:nsub+1) = get_subs_from_cluster_ids(buffer,subbuffer,k,ids(first:nsub),n,evals) 
        CASE DEFAULT ! Equally spaced subintervals
            subs(first:nsub+1) = get_linspace(evals(1)-buffer,evals(n)+buffer,nsub+2-first)
        END SELECT    
    END SUBROUTINE compute_subintervals


    !> \brief If any almost degenerate (within given `thresh` value) eigenvalues are in different clusters,
    !> smaller eigenvalue is merged into the cluster of the larger eigenvalue.
    SUBROUTINE fix_cluster_ids(ids,evals,thresh)
        IMPLICIT NONE
        INTEGER, INTENT(INOUT)       :: ids(:)
        REAL(dp), INTENT(IN)          :: evals(:)
        REAL(dp), INTENT(IN)          :: thresh
        INTEGER  :: i, k  
        INTEGER  :: oldids(SIZE(ids))

        oldids = 0
        k = SIZE(ids)
        DO WHILE (ANY(ids-oldids .NE. 0))    
            oldids = ids
            DO i = 1, k-1
                IF (evals(ids(i+1)) - evals(ids(i+1)-1) < thresh) THEN
                    ids(i+1) = ids(i+1) - 1 
                END IF
            END DO
        END DO
    END SUBROUTINE fix_cluster_ids

 
    !> \brief Extends interval to include missing eigenvalues
    SUBROUTINE extend_interval(nsub,extopt,extwidth,subs)
        IMPLICIT NONE
        INTEGER, INTENT(IN)     :: nsub             !> Number of subintervals
        INTEGER, INTENT(IN)     :: extopt           !> Extension option (0 for both sides, 1 for RHS, -1 for LHS
        REAL(dp), INTENT(IN)    :: extwidth         !> Extension width
        REAL(dp), INTENT(INOUT) :: subs(nsub+1)     !> Subintervals
        INTEGER             :: first

        ! Check if the left interval is -infinity
        IF (subs(1) > -1.D10) THEN 
            first = 1
        ELSE ! -inf
            first = 2
        END IF
        IF (extopt == 0) THEN
            subs(first)  = subs(first)  - extwidth
            subs(nsub+1) = subs(nsub+1) + extwidth
        ELSE IF (extopt < 0) THEN
            subs(first)  = subs(first)  - extwidth
        ELSE IF (extopt > 0) THEN
            subs(nsub+1) = subs(nsub+1) + extwidth
        END IF 
    END SUBROUTINE extend_interval


    !> \brief Extends subintervals to include missing eigenvalues
    SUBROUTINE extend_subintervals(nsub,fixed,extwidth,subs)
        IMPLICIT NONE
        INTEGER, INTENT(IN)     :: nsub         !> Number of subintervals
        INTEGER, INTENT(IN)     :: fixed        !> Subint index that will be fixed, 1,2, or nsub + 1
        REAL(dp), INTENT(IN)    :: extwidth     !> Extension width
        REAL(dp), INTENT(INOUT) :: subs(nsub+1) !> Subintervals
        IF (fixed == nsub+1) THEN
            subs(:fixed-1) = subs(:fixed-1) - extwidth
        ELSE IF (fixed < 3) THEN
            subs(fixed+1:) = subs(fixed+1:) + extwidth
        ELSE
            STOP 'fixed should be 1, 2 or nsub+1'
        END IF 
    END SUBROUTINE extend_subintervals

END MODULE
