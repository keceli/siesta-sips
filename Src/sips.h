#ifndef SIPS_H
#define SIPS_H

#include <slepceps.h>
#include <petsc/private/petscimpl.h>
PetscErrorCode EPSKrylovSchurGetLocalInterval(EPS eps,PetscReal **interval,PetscInt **inertia);
PetscErrorCode EPSKrylovSchurGetSubComm(EPS eps,MPI_Comm *matComm,MPI_Comm *epsComm);
PetscErrorCode epscreatedensitymat(EPS eps,PetscReal *weight,PetscInt idx_start,PetscInt idx_end,Mat *P);
PetscErrorCode epscreatedensitymat2(EPS eps,PetscReal *weight,PetscInt idx_start,PetscInt idx_end,Mat *P,Mat *P_2);
PetscErrorCode MatMatMultGetTrace(EPS eps,Mat P,Mat B,PetscInt sgn,Vec vdiag,PetscReal *tr);
PetscErrorCode getF(PetscInt *atomids,Mat T,Mat D,Mat GH1,Mat GH2, Mat *F);
int hello(int idx_start);

#endif/*SIPS_H*/
