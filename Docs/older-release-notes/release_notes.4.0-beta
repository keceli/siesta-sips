Release Notes for Siesta-4.0-beta (December 2015)

This version includes the van der Waals functionals, the new
load-balancing code for real-space grid operations, a Wannier90
interface, a new Orbital-Minimization-Method solver, and other
improvements and bug fixes that have been part of the development
version for some time. Its feature set has been frozen for several
months and it has undergone enough testing to be considered a
near-production version.

Please see the TECHNICAL NOTES section below for some important issues
regarding backwards compatibility with 3.X versions and previous
development snapshots.

See the file REPORTING_BUGS for instructions to report bugs and
suggestions.


-*- NEW FEATURES

Please see the relevant section of the manual for more information.

* New SiestaXC library for exchange and correlation, implementing several
  van der Waals functionals and some newer GGA ones.

* New code to improve the load-balancing of the operations in the
  real-space grid when running in parallel.

* A new interface to the Wannier90 code for the generation of
  maximally localized wannier functions.

* New electronic-structure solver implementing the the Orbital-Minimization-Method

* New mixing options, including hamiltonian and charge-density mixing

* Charge-confinement and "filteret" basis-set generation options.

* Improved MPI version of the Siesta-as-subroutine code for executing
  independent calculations.

* New JobList utility to organize and run multiple jobs.

* Timer with call-tree awareness.

* Performance and usability enhancements in TranSiesta.

* Enhancements to the restart capabilities in molecular-dynamics runs.

* New options for wave-function output. New WFSX format.

* 'Fatbands' analysis.

* Enhancements to the COOP/COHP analyzer

* Hirshfeld and Voronoi charges. Bader analysis output

* Force-convergence diagnostics.

* New HSX file format for Hamiltonian/Overlap files

* Calculation of the vacuum level for non-bulk systems.

* Updates to other analysis tools in Util/

* Replacement of license-encumbered routines by new ones.

* Enhancements to the manual and build system.

In addition, a number of bugs have been fixed, and there have been
numerous cosmetic changes to the output and the code itself.


-*- TECHNICAL NOTES

Please take into account the following changes in behavior.


-- Changes with respect to the behavior of the of the Siesta 3.X versions

(These have been in effect for all the development snapshots 
after the 3.0 official release)

*** The grid functions (charge densities, potentials, etc) were in
single precision by default in the 3.X versions, but are in double
precision by default for post-3.X versions. The 'phi' array that holds
the values of the basis orbitals on the real-space grid is kept in
single precision. Please take this into account if you compare the
results with those of siesta-3.X runs. See the manual in both versions
for more information.


-- Changes with respect to the latest development snapshot released on
the Siesta website (siesta-trunk-462):

*** Changes in the geometry used for the analysis of the electronic
structure, as well as in the handling of the density matrix (DM) and
hamiltonian (H). This will _slightly_ change the output of most
calculations and the detailed results of any post-processing. Keep
this in mind if you need to maintain coherency within a project.

Please see the file Changelog-from-trunk-462.txt in this directory
for more information.


